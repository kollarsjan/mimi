import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/**
 * This class composes a Region which provides controls to zoom an <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/image/ImageView.html">javafx ImageView</a>
 * within a <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ScrollPane.html">javafx ScrollPane</a>.
 * Two Buttons are created which set the ImageView on 100% of its original size or the be contained within the Pane.
 * The size can also be controlled with the Slider.
 * As a bonus two Sliders will be created which can transform it on the x and y axis.
 *
 * @author Jan Kollars
 */
public class Zoom extends Region {
    private ImageView imageView;
    private ScrollPane scrollPane;

    private Slider sizeSlider;

    /**
     * Construct the zoom controls for a specific ImageView and a specific ScrollPane.
     * @param iv is the ImageView which should be zoomed
     * @param sp is the ScrollPane in which the ImageView is located
     */
    Zoom (ImageView iv, ScrollPane sp) {
        imageView = iv;
        scrollPane = sp;
        GridPane zoomPane = new GridPane();
        zoomPane.getStyleClass().add("zoom-pane");

        Label zoomLabel = new Label("Zoom settings");
        Button originSizeBtn = new Button("100%");
        Button containBtn = new Button("Contain");
        sizeSlider = new Slider(.1, 4, 1);

        Label transformLabel = new Label("Transform");
        Label tXLabel = new Label("X");
        Label tYLabel = new Label("Y");
        Slider tXSlider = new Slider(0, 500, 0);
        Slider tYSlider = new Slider(0, 500, 0);

        originSizeBtn.setOnAction(event -> {
            resizeFullSize();
            readjustSlider();
        });
        containBtn.setOnAction(event -> {
            resizeContain();
            readjustSlider();
        });
        sizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> freeResize(newValue));

        zoomPane.add(zoomLabel, 0, 0);
        zoomPane.add(originSizeBtn, 0, 1);
        zoomPane.add(containBtn, 1, 1);
        zoomPane.add(sizeSlider, 0, 2, 2, 1);
        zoomPane.add(transformLabel, 0, 3, 2, 1);
        zoomPane.add(tXLabel, 0, 4);
        zoomPane.add(tXSlider, 1, 4);
        zoomPane.add(tYLabel, 0, 5);
        zoomPane.add(tYSlider, 1, 5);

        // move ImageView by slider value
        imageView.translateXProperty().bind(tXSlider.valueProperty());
        imageView.translateYProperty().bind(tYSlider.valueProperty());

        this.getChildren().add(zoomPane);
    }

    /** reset the ImageView to size of Image */
    private void resizeFullSize () {
        if (imageView.getImage() != null) imageView.setFitWidth(getOriginalWidth());
    }

    /** resize the ImageView that it is contained in the ScrollPane */
    private void resizeContain () {
        if (imageView.getImage() != null && widthIsConstraint()) {
            imageView.setFitWidth(scrollPane.getWidth() - 2);
        } else if (imageView.getImage() != null) {
            imageView.setFitHeight(scrollPane.getHeight() - 2);
        }
    }

    /**
     * resize the ImageView by a factor
     * @param value the factor by which the ImageView should be resized
     */
    private void freeResize (Number value) {
        if (imageView.getImage() != null) imageView.setFitWidth((double) value * getOriginalWidth());
    }

    /** triggers a recalculation of the Slider value corresponding to the ImageView size */
    private void readjustSlider () {
        if (imageView.getImage() != null) sizeSlider.setValue(imageView.getFitWidth() / getOriginalWidth());
    }

    /**
     * compares the image's aspect ratio and the ScrollPanel ratio
     * @return the width is the constraint; returns false if no image is set
     */
    private boolean widthIsConstraint () {
        if (imageView.getImage() != null) return getOriginalWidth()/getOriginalHeight() >= scrollPane.getWidth()/scrollPane.getHeight();
        return false;
    }

    /**
     * gets the Image's original width as number of pixels
     * @return the Image's original width as number of pixels
     */
    private double getOriginalWidth () {
        return imageView.getImage().getWidth();
    }

    /**
     * gets the Image's original height as number of pixels
     * @return the Image's original height as number of pixels
     */
    private double getOriginalHeight () {
        return imageView.getImage().getHeight();
    }

}
