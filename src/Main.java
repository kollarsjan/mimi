import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * MIMI (Mini Image Manipulation Interface
 * This is the class which creates the application window, the main layout and adds many controls partly by creating instances of other classes.
 *
 * @author Jan Kollars
 */
public class Main extends Application {

    final private Slider sliderR = new Slider(-255, 255, 0);
    final private Slider sliderG = new Slider(-255, 255, 0);
    final private Slider sliderB = new Slider(-255, 255, 0);
    final private Slider[] sliders = new Slider[3];

    private FileChooser fileChooser;
    private Image image;
    final private ImageView imageView = new ImageView();
    private PixelReader pixelReader;
    private PixelWriter pixelWriter;
    private WritableImage wImage;
    private ScrollPane outputPane = new ScrollPane(imageView);

    /** contains the manipulated color values */
    private int[][] newValues;

    /** luminance Histogram */
    private Histogram hL;
    /** red Histogram */
    private Histogram hCR;
    /** green Histogram */
    private Histogram hCG;
    /** blue Histogram */
    private Histogram hCB;

    @Override
    public void start(Stage stage) throws Exception {
        // setup panes
        GridPane root = new GridPane();

        ColumnConstraints column1 = new ColumnConstraints(300);
        ColumnConstraints column2 = new ColumnConstraints();
        ColumnConstraints column3 = new ColumnConstraints(300);
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();

        column2.setHgrow(Priority.ALWAYS);
        row2.setVgrow(Priority.ALWAYS);
        root.getColumnConstraints().addAll(column1, column2, column3);
        root.getRowConstraints().addAll(row1, row2);

        FlowPane settingsPane = new FlowPane();
        FlowPane inputPane = new FlowPane();
        FlowPane histogramPane = new FlowPane();

        root.getStyleClass().add("outer-pane");
        settingsPane.getStyleClass().add("settings-pane");
        inputPane.getStyleClass().add("input-pane");
        outputPane.getStyleClass().add("output-pane");
        histogramPane.getStyleClass().add("histogram-pane");

        root.add(settingsPane, 0, 0, 1, 2);
        root.add(inputPane, 1, 0);
        root.add(outputPane, 1, 1);
        root.add(histogramPane, 2, 0, 1, 2);

        Scene scene = new Scene(root, 1300, 600, Color.WHITE);
        scene.getStylesheets().add("styles.css");
        stage.setTitle("Aufgabe 8");
        stage.setMinHeight(620);
        stage.setMinWidth(800);
        stage.setScene(scene);
        stage.show();

        // populate settingsPane
        // colorization controls
        GridPane colorizePane = new GridPane();
        colorizePane.getStyleClass().add("colorize-pane");

        sliders[0] = sliderR;
        sliders[1] = sliderG;
        sliders[2] = sliderB;

        final Label labelR = new Label("R");
        final Label labelG = new Label("G");
        final Label labelB = new Label("B");

        final Label valueR = new Label();
        final Label valueG = new Label();
        final Label valueB = new Label();

        valueR.textProperty().bind(Bindings.format("%.2f", sliderR.valueProperty()));
        valueG.textProperty().bind(Bindings.format("%.2f", sliderG.valueProperty()));
        valueB.textProperty().bind(Bindings.format("%.2f", sliderB.valueProperty()));

        colorizePane.add(labelR, 0, 0);
        colorizePane.add(sliderR, 1, 0);
        colorizePane.add(valueR, 2, 0);
        colorizePane.add(labelG, 0, 1);
        colorizePane.add(sliderG, 1, 1);
        colorizePane.add(valueG, 2, 1);
        colorizePane.add(labelB, 0, 2);
        colorizePane.add(sliderB, 1, 2);
        colorizePane.add(valueB, 2, 2);

        // zoom controls
        Zoom zoom = new Zoom(imageView, outputPane);

        // effect controls
        Effects effects = new Effects(imageView);

        settingsPane.getChildren().addAll(colorizePane, zoom, effects);

        // add Listeners to Color sliders
        for (Slider slider : sliders) {
            slider.valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    colorize((int) sliderR.getValue(), (int) sliderG.getValue(), (int) sliderB.getValue());             // fires on drag

                    if (oldValue != newValue && !slider.isValueChanging() && image != null) {                           // fires on click and on left/right key press
                        hL.update(newValues);
                        hCR.update(newValues);
                        hCG.update(newValues);
                        hCB.update(newValues);
                    }
                }
            });
            slider.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(
                        ObservableValue<? extends Boolean> observableValue,
                        Boolean wasChanging,
                        Boolean changing)
                {
                    if (wasChanging && image != null) {                                                                 // fires on drag end and click on slider knob without change (unintended)
                        hL.update(newValues);
                        hCR.update(newValues);
                        hCG.update(newValues);
                        hCB.update(newValues);
                    }
                }
            });
        }

        // populate inputPane
        fileChooser = new FileChooser();
        fileChooser.setTitle("Open a Picture ...");

        Button openBtn = new Button("Open a Picture ...");
        Button saveBtn = new Button("Save Picture ...");
        ToggleButton hideHistoBtn = new ToggleButton("Hide histograms");

        openBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent event) {
                File file = fileChooser.showOpenDialog(stage);
                if (file != null) {
                    openFile(file);
                    colorize((int) sliderR.getValue(), (int) sliderG.getValue(), (int) sliderB.getValue());
                    hL.update(newValues);
                    hCR.update(newValues);
                    hCG.update(newValues);
                    hCB.update(newValues);
                    saveBtn.setDisable(false);
                }
            }
        });

        saveBtn.setDisable(true);
        saveBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                File file = fileChooser.showSaveDialog(stage);
                if (file != null) {
                    try {
                        ImageIO.write(SwingFXUtils.fromFXImage(imageView.snapshot(null, null), null), "png", file);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        hideHistoBtn.setOnAction(event -> {
            column3.setPrefWidth(hideHistoBtn.isSelected() ? 0 : 300);
        });

        inputPane.getChildren().addAll(openBtn, saveBtn, hideHistoBtn);

        // populate histogramPane
        hL = new Histogram("Luminance");
        hCR = new Histogram("Red");
        hCG = new Histogram("Green");
        hCB = new Histogram("Blue");

        histogramPane.getChildren().addAll(hL, hCR, hCG, hCB);
    }

    /**
     * Helper function which performs the insertion of the selected File into the ImageView.
     * @param file is the File which should be inserted in the ImageView.
     */
    private void openFile (File file) {
        image = new Image(file.toURI().toString());
        wImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        pixelReader = image.getPixelReader();
        pixelWriter = wImage.getPixelWriter();
        imageView.setImage(image);
        imageView.setPreserveRatio(true);
        imageView.setCache(true);
    }

    /**
     * This method manipulates the image's color values.
     * @param redValue   is the value which should be applied to the image's red color values
     * @param greenValue is the value which should be applied to the image's green color values
     * @param blueValue  is the value which should be applied to the image's blue color values
     */
    private void colorize (int redValue, int greenValue, int blueValue) {
        if (image != null) {
            int pixelsY = (int) image.getHeight();
            int pixelsX = (int) image.getWidth();
            newValues = new int[pixelsX*pixelsY][3];
            int i = 0;                                                                                                  // holds the index of the current pixel which is being processed
            for (int y = 0; y < pixelsY; y++) {
                for (int x = 0; x < pixelsX; x++) {
                    int myRGB = pixelReader.getArgb(x, y);
                    int red = (myRGB >> 16) & 0xFF;
                    int green = (myRGB >> 8) & 0xFF;
                    int blue = myRGB & 0xFF;

                    int newRed = calcNewColor(red, redValue);
                    int newGreen = calcNewColor(green, greenValue);
                    int newBlue = calcNewColor(blue, blueValue);

                    int newRGB = 0xFF000000 | (newRed << 16) | (newGreen << 8) | newBlue;
                    pixelWriter.setArgb(x, y, newRGB);
                    newValues[i][0] = newRed;
                    newValues[i][1] = newGreen;
                    newValues[i][2] = newBlue;
                    i++;
                }
            }
            imageView.setImage(wImage);
        }
    }

    /**
     * Helper function which calculates the new modified color value (adding/subtracting) using the modifying value and the old color value.
     * It makes sure that the calculation's result is in between 0 and 255.
     * @param oldValue is the color value which should be modified.
     * @param change   is the value which should be added (negative values will be subtracted).
     * @return         a value which is in between 0 and 255
     */
    private int calcNewColor(int oldValue, int change) {
        int newValue = oldValue + change;
        if (newValue > 255) {
            newValue = 255;
        } else if (newValue < 0) {
            newValue = 0;
        }
        return newValue;
    }

    /** this is the program's entry point */
    public static void main (String[] args) {
        launch(args);
    }
}
