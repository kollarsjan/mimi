import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/**
 * This class composes a Region which provides controls to apply effect to an <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/image/ImageView.html">javafx ImageView</a>.
 * Gaussian blur, Reflection and Drop shadow effects will be created.
 * @author Jan Kollars
 */
public class Effects extends Region {

    /**
     * constructs a Region with controls that apply effects to an ImageView
     * @param iv is the ImageView which should be affected by the effects
     */
    Effects(ImageView iv) {
        GridPane effectsPane = new GridPane();
        effectsPane.getStyleClass().add("effects-pane");

        Label effectsLabel = new Label("Effects controls");

        Label gaussianLabel = new Label("Gaussian Blur");
        Label gRadiusLabel = new Label("Radius");
        Slider gRadiusSlider = new Slider(0, 20, 0);

        Label reflectionLabel = new Label("Reflection");
        Label rFractionLabel = new Label("Reveal");
        Label rOffsetLabel = new Label("Offset");
        Slider rFractionSlider = new Slider(0, 1, 0);
        Slider rOffsetSlider = new Slider(0, 50, 0);

        Label dropShadowLabel = new Label("Drop Shadow");
        Label dsOffsetXLabel = new Label("Offset X");
        Label dsOffsetYLabel = new Label("Offset Y");
        Label dsRadiusLabel = new Label("Radius");
        Slider dsOffsetXSlider = new Slider(-50, 50, 0);
        Slider dsOffsetYSlider = new Slider(-50, 50, 0);
        Slider dsRadiusSlider = new Slider(0, 127, 0);

        effectsPane.add(effectsLabel, 0, 0, 2, 1);
        effectsPane.add(gaussianLabel, 0, 1, 2, 1);
        effectsPane.add(gRadiusLabel, 0, 2);
        effectsPane.add(gRadiusSlider, 1, 2);
        effectsPane.add(reflectionLabel, 0, 3, 2, 1);
        effectsPane.add(rFractionLabel, 0, 4);
        effectsPane.add(rFractionSlider, 1, 4);
        effectsPane.add(rOffsetLabel, 0, 5);
        effectsPane.add(rOffsetSlider, 1, 5);
        effectsPane.add(dropShadowLabel, 0, 6, 2, 1);
        effectsPane.add(dsOffsetXLabel, 0, 7);
        effectsPane.add(dsOffsetXSlider, 1, 7);
        effectsPane.add(dsOffsetYLabel, 0, 8);
        effectsPane.add(dsOffsetYSlider, 1, 8);
        effectsPane.add(dsRadiusLabel, 0, 9);
        effectsPane.add(dsRadiusSlider, 1, 9);

        GaussianBlur gb = new GaussianBlur();
        Reflection r = new Reflection();
        DropShadow ds = new DropShadow();

        gb.radiusProperty().bind(gRadiusSlider.valueProperty());

        r.fractionProperty().bind(rFractionSlider.valueProperty());
        r.topOffsetProperty().bind(rOffsetSlider.valueProperty());

        ds.offsetXProperty().bind(dsOffsetXSlider.valueProperty());
        ds.offsetYProperty().bind(dsOffsetYSlider.valueProperty());
        ds.radiusProperty().bind(dsRadiusSlider.valueProperty());

        r.setInput(ds);                                                                                                 // chained effects
        gb.setInput(r);
        iv.setEffect(gb);

        this.getChildren().add(effectsPane);
    }
}
