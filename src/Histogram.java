import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Region;

/**
 * This class composes a Region which contains an Histogram using the <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/chart/BarChart.html">javafx BarChart</a>.
 * The class can create 4 different types of Histograms ("Luminance", "Red", "Green", "Blue).
 *
 * @author Jan Kollars
 */
public class Histogram extends Region {
    // necessary instances for the javafx BarChart
    final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();
    final BarChart<String, Number> bc = new BarChart<String, Number>(xAxis,yAxis);
    XYChart.Series series;
    /** holds the type of the Histogram, being either "Luminance", "Red", "Green" or "Blue" */
    private String type;

    /**
     * Construct a new Histogram with a certain type
     * @param type the Histogram's type (accepts "Luminance", "Red", "Green", "Blue")
     */
    public Histogram (String type) {
        this.type = type;
        bc.setTitle(type);
        bc.setMaxHeight(100);
        bc.setMaxWidth(300);

        xAxis.setTickLabelsVisible(false);
        yAxis.setTickLabelsVisible(false);
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0);

        series = new XYChart.Series();
        bc.getData().add(series);

        this.getChildren().add(bc);
        this.getStyleClass().add("histogram");
        this.getStyleClass().add("histogram--" + type.toLowerCase());
        System.out.println("histogram--" + type.toLowerCase());
    }

    /**
     * Analyse the distribution of the provided pixel data and renders the result in the Chart.
     * @param colors is a multidimentional Array containing the RGB values of every pixel, which has the following structure: colors[pixelNr][RedValue, GreenValue, BlueValue].
     */
    public void update (int[][] colors) {
        series.getData().removeAll();

        int[] values = new int[256];
        int max = 0;
        for (int i = 0; i < colors.length; i++) {                                                                       // iterate over pixels and extract relevant L/C value
            switch (type) {
                case "Luminance":
                    double lumVal = 0.299*colors[i][0] + 0.587*colors[i][1] + 0.114*colors[i][2];                       // calculates the pixel's luminance
                    values[(int) Math.round(lumVal)]++;
                    break;
                case "Red":
                    values[colors[i][0]]++;
                    break;
                case "Green":
                    values[colors[i][1]]++;
                    break;
                case "Blue":
                    values[colors[i][2]]++;
                    break;
            }
        }

        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {                                                                                      // if new peak
                max = values[i];
                yAxis.setUpperBound(max);
            }
            series.getData().add(new XYChart.Data<String, Number>(Integer.toString(i), values[i]));                     // add value to chart
        }
    }
}
